<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


///* Auto-generated admin routes */
////Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//Route::middleware([])->group(static function () {
//    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
//            Route::get('/',                                             'AdminUsersController@index')->name('index');
//            Route::get('/create',                                       'AdminUsersController@create')->name('create');
//            Route::post('/',                                            'AdminUsersController@store')->name('store');
//            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
//            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
//            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
//            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
//            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
//        });
//    });
//});
//
///* Auto-generated admin routes */
////Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//Route::middleware([])->group(static function () {
//    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
//        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
//        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
//        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
//    });
//});

//Route::middleware(['web', 'auth.custom'])->group(static function () {
//    Route::namespace('Brackets\AdminAuth\Http\Controllers')->group(static function () {
//        Route::get('/admin', 'AdminHomepageController@index')->name('brackets/admin-auth::admin');
//    });
//});


Route::namespace('App\Http\Controllers')->group(static function () {
    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
    Route::any('/admin/logout', 'Auth\LoginController@logout')->name('brackets/admin-auth::admin/logout');
    Route::get('/admin', 'Admin\ApplicationsController@main')->name('brackets/admin-auth::admin');
});


/* Auto-generated admin routes */
Route::middleware(['auth.custom'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('applications')->name('applications/')->group(static function() {
            Route::get('/',                                             'ApplicationsController@index')->name('index');
            Route::get('/create',                                       'ApplicationsController@create')->name('create');
            Route::post('/',                                            'ApplicationsController@store')->name('store');
            Route::get('/{application}/edit',                           'ApplicationsController@edit')->name('edit');
            Route::get('/{application}/reject',                         'ApplicationsController@reject')->name('reject');
            Route::post('/bulk-destroy',                                'ApplicationsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{application}',                               'ApplicationsController@update')->name('update');
            Route::delete('/{application}',                             'ApplicationsController@destroy')->name('destroy');
        });
        Route::get('/content',                                           'ApplicationsController@content')->name('content');
    });
});
