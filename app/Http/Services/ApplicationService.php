<?php

namespace App\Http\Services;

class ApplicationService
{
    /**
     * @var ClientService
     */
    private $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    public function getApplications($data)
    {
        $applications = $this->clientService->get('applications', $data);
        return $applications;
    }

    public function getApplication($id, $data)
    {
        $application = $this->clientService->get('applications/'.$id, $data);
        return $application;
    }

    public function updateApplication(int $id, array $data)
    {
        $type = '';
        $state = $data['state'];
        if ($state == 1) {
            $type = '/initial';
        } elseif ($state == 2 && $data['bank'] == null) {
            $data = ['meeting' => $data['meeting']];
        } elseif ($state == 2) {
            $type = '/meeting';
        }

        $application = $this->clientService->put('applications/'.$id.$type, $data);
        return $application;
    }

    public function assignApplication(int $id)
    {
        $application = $this->clientService->put('applications/'.$id.'/assign');
        return $application;
    }

    public function rejectApplication(int $id)
    {
        $application = $this->clientService->put('applications/'.$id.'/reject');
        return $application;
    }

    public function createApplication(array $data)
    {
        $application = $this->clientService->post('applications', $data);
        return $application;
    }

    public function getPartnerApplication()
    {
        $application = $this->clientService->get('applications/partner/application');
        return $application;
    }

}
