<?php

namespace App\Http\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class ClientService
{
    /**
     * @var Client
     */
    private $client;


    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('services.base_uri'),
            'headers' => [
                'Authorization' => 'Bearer '.Cache::get('token')
            ]
        ]);
    }

    public function get($uri, $data = [])
    {
        $response = $this->client->get($uri, ['query' => $data]);
        $response = json_decode($response->getBody()->getContents(), true);
        return $response;
    }

    public function post($uri, $data = [])
    {
        $response = $this->client->post($uri, ['form_params' => $data]);
        $response = json_decode($response->getBody()->getContents(), true);
        return $response;
    }

    public function put($uri, $data = [])
    {
        $response = $this->client->put($uri, ['form_params' => $data]);
        $response = json_decode($response->getBody()->getContents(), true);
        return $response;
    }
}
