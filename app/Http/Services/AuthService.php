<?php

namespace App\Http\Services;

class AuthService
{
    /**
     * @var ClientService
     */
    private $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    public function login($data)
    {
        $response = $this->clientService->post('login', $data);
        return $response;
    }

    public function register($data)
    {
        $response = $this->clientService->post('register', $data);
        return $response;
    }
}
