<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Application\BulkDestroyApplication;
use App\Http\Requests\Admin\Application\ContentApplication;
use App\Http\Requests\Admin\Application\DestroyApplication;
use App\Http\Requests\Admin\Application\IndexApplication;
use App\Http\Requests\Admin\Application\StoreApplication;
use App\Http\Requests\Admin\Application\UpdateApplication;
use App\Http\Services\ApplicationService;
use App\Models\Application;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ApplicationsController extends Controller
{
    /**
     * @var ApplicationService
     */
    private $applicationService;

    public function __construct(ApplicationService $applicationService)
    {
        $this->applicationService = $applicationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexApplication $request
     * @return array|Factory|View
     */
    public function index(IndexApplication $request)
    {
//        // create and AdminListing instance for a specific model and
//        $data = AdminListing::create(Application::class)->processRequestAndGet(
//            // pass the request with params
//            $request,
//
//            // set columns to query
//            ['id', 'company', 'state', 'partner', 'manager'],
//
//            // set columns to searchIn
//            ['id', 'company', 'state', 'partner', 'manager']
//        );

        $user_type = Cache::get('user_type');
        if ($user_type == 'partner') {
            $application = $this->applicationService->getPartnerApplication()['data']['application'];
            if ($application == null) {
                return redirect('admin/applications/create');
            } else {
                $id = $application['id'];
                return redirect("admin/applications/$id/edit");
            }

        }

        $data = [
            'filter' => $request->get('filter')
        ];

        $applications = $this->applicationService->getApplications($data)['data']['applications'];

        $data = $this->getPaginatedResult($applications, 5);

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.application.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
//        $this->authorize('admin.application.create');

        return view('admin.application.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreApplication $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreApplication $request)
    {
        // Sanitize input
//        $sanitized = $request->getSanitized();

        // Store the Application
//        $application = Application::create($sanitized);

        $data = [
            'company' => $request->post('company'),
            'address' => $request->post('address'),
            'bin' => $request->post('bin')
        ];

        $application = $this->applicationService->createApplication($data);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applications')];
        }

        return redirect('admin/applications');
    }

    /**
     * Display the specified resource.
     *
     * @param Application $application
     * @throws AuthorizationException
     * @return void
     */
    public function show(Application $application)
    {
        $this->authorize('admin.application.show', $application);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Application $application
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(int $id)
    {
//        $this->authorize('admin.application.edit', $application);

        $application = $this->applicationService->getApplication($id, [])['data']['application'];
        $application['manager']['email'] = $application['manager']['email'] ?? 'unassigned';
        $application['isSetDate'] = $application['meeting'] == null ? true : false;
        $application['user_type'] = Cache::get('user_type');
        $application = (object)$application;

        return view('admin.application.edit', [
            'application' => $application,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateApplication $request
     * @param Application $application
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateApplication $request, int $id)
    {
        if ($request->post('manager')['email'] == 'unassigned'){
            $application = $this->applicationService->assignApplication($id);
            return [
                'redirect' => url('admin/applications/'.$id.'/edit'),
            ];
        }

        $data = $request->all();

        $application = $this->applicationService->updateApplication($id, $data);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applications/'.$id.'/edit'),
            ];
        }
    }

    public function reject(UpdateApplication $request, int $id)
    {
        $application = $this->applicationService->rejectApplication($id);

        return redirect('admin/applications/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyApplication $request
     * @param Application $application
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyApplication $request, Application $application)
    {
        $application->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyApplication $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyApplication $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Application::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    public function content(ContentApplication $request, ApplicationService $applicationService)
    {
        $isActive = $applicationService->getPartnerApplication()['data']['is_active'];
        return view('admin.layout.content', ['is_active' => $isActive]);
    }

    public function main() {
        return view('admin.layout.main');
    }

    private function getPaginatedResult(array $result = [], $perPage = 100)
    {
        $total = count($result);
        $paginator = new LengthAwarePaginator(collect($result), count($result), $perPage);
        $start = ($paginator->currentPage() - 1) * $perPage;
        $sliced = array_slice($result, $start, $perPage);

        return new LengthAwarePaginator($sliced, $total, $perPage);
    }
}
