<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Services\AuthService;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * Create a new controller instance.
     *
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->middleware('guest')->except('logout');
        $this->authService = $authService;
    }

    public function login(Request $request)
    {
        $data = $request->all();
        $response = $this->authService->login($data);

        if ($response['status']==200) {
            Cache::put('token', $response['data']['token'], now()->addDay());
            Cache::put('user_type', $response['data']['user_type'], now()->addDay());
            return redirect()->route('brackets/admin-auth::admin');
        } else {
            return redirect()->route('login');
        }
    }

    public function logout(Request $request)
    {
        Cache::forget('token');
        return redirect()->route('login');
    }
}
