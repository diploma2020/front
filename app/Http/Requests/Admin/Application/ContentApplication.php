<?php

namespace App\Http\Requests\Admin\Application;

use App\Http\Services\ApplicationService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Gate;

class ContentApplication extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(ApplicationService $applicationService): bool
    {
//        $is_active = $applicationService->getPartnerApplication()['data']['is_active'];
//
//        if ($is_active == true) {
//            return true;
//        }
//        return Gate::allows('admin.application.index');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
//            'orderBy' => 'in:id,company,state,partner,manager|nullable',
//            'orderDirection' => 'in:asc,desc|nullable',
//            'search' => 'string|nullable',
//            'page' => 'integer|nullable',
//            'per_page' => 'integer|nullable',

        ];
    }
}
