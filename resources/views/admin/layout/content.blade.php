@extends('brackets/admin-ui::admin.layout.default')

@section('body')

    <div class="welcome-quote">

        @if($is_active)
            <h1>Content page is available</h1>
        @else
            <h1>Functionality not available yet, complete onboarding first</h1>
        @endif

    </div>

@endsection
