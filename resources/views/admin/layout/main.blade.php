@extends('brackets/admin-ui::admin.layout.default')

@section('body')

    <div class="welcome-quote">

        <blockquote>
            {{'Welcome to '.\Illuminate\Support\Facades\Cache::get('user_type').' panel'}}
        </blockquote>

    </div>

@endsection
