@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.application.actions.edit', ['name' => $application->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <application-form
{{--                :action="'applications/{{ $application->id }}'"--}}
                :action="'{{ url('admin/applications').'/'.$application->id }}'"
{{--                :data="{{ $application->toJson() }}"--}}
                :data="{{ json_encode($application) }}"
                v-cloak
                inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>

                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ $application->uuid }}
                    </div>
                    <div class="card-body">
                        @include('admin.application.components.form-edit')
                    </div>

                    <div v-show="{{$application->state < 3 && $application->is_rejected == false && $application->user_type == 'manager'}}"  class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            {{
                                $application->manager['email'] == 'unassigned'
                                ? 'Assign' : 'Next state'
                            }}
                        </button>
                        <a v-show="{{$application->manager['email'] != 'unassigned' && $application->state < 3}}" class="btn btn-primary" href="{{url('admin/applications').'/'.$application->id.'/reject'}}">Reject</a>
                    </div>

                </form>

        </application-form>

        </div>

</div>

@endsection
