<h3 v-show="(form.state > 0)">Review</h3>

<div v-show="form.state > 0" class="form-group row align-items-center" :class="{'has-danger': errors.has('partner'), 'has-success': fields.partner && fields.partner.valid }">
    <label for="partner" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.partner') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.partner.email" :disabled="true" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('partner'), 'form-control-success': fields.partner && fields.partner.valid}" id="partner" name="partner" placeholder="{{ trans('admin.application.columns.partner') }}">
        <div v-if="errors.has('partner')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('partner') }}</div>
    </div>
</div>

<div v-show="form.state > 0" class="form-group row align-items-center" :class="{'has-danger': errors.has('manager'), 'has-success': fields.manager && fields.manager.valid }">
    <label for="manager" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.manager') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.manager.email" :disabled="true" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('manager'), 'form-control-success': fields.manager && fields.manager.valid}" id="manager" name="manager" placeholder="{{ trans('admin.application.columns.manager') }}">
        <div v-if="errors.has('manager')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('manager') }}</div>
    </div>
</div>

<div v-show="form.state > 0" class="form-group row align-items-center" :class="{'has-danger': errors.has('company'), 'has-success': fields.company && fields.company.valid }">
    <label for="company" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.company') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.company" :disabled="form.state > 1 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('company'), 'form-control-success': fields.company && fields.company.valid}" id="company" name="company" placeholder="{{ trans('admin.application.columns.company') }}">
        <div v-if="errors.has('company')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('company') }}</div>
    </div>
</div>

<div v-show="form.state > 0" class="form-group row align-items-center" :class="{'has-danger': errors.has('address'), 'has-success': fields.address && fields.address.valid }">
    <label for="address" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.address') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.address" :disabled="form.state > 1 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('address'), 'form-control-success': fields.address && fields.address.valid}" id="address" name="address" placeholder="{{ trans('admin.application.columns.address') }}">
        <div v-if="errors.has('address')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('address') }}</div>
    </div>
</div>

<div v-show="form.state > 0" class="form-group row align-items-center" :class="{'has-danger': errors.has('bin'), 'has-success': fields.bin && fields.bin.valid }">
    <label for="bin" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.bin') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.bin" :disabled="form.state > 1 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('bin'), 'form-control-success': fields.bin && fields.bin.valid}" id="bin" name="bin" placeholder="{{ trans('admin.application.columns.bin') }}">
        <div v-if="errors.has('bin')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bin') }}</div>
    </div>
</div>

<h3 v-show="form.state > 1 && !form.is_rejected">Meeting</h3>

{{--<div v-show="form.state > 1" class="form-group row align-items-center" :class="{'has-danger': errors.has('meeting'), 'has-success': fields.meeting && fields.meeting.valid }">--}}
{{--    <label for="meeting" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.meeting') }}</label>--}}
{{--    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">--}}
{{--        <div class="input-group input-group--custom">--}}
{{--            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>--}}
{{--            <datetime v-model="form.meeting" :disabled="form.state > 2 || form.isSetDate == false || form.is_rejected == true" :config="datetimePickerConfig" class="flatpickr" :class="{'form-control-danger': errors.has('meeting'), 'form-control-success': fields.meeting && fields.meeting.valid}" id="meeting" name="meeting" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>--}}
{{--        </div>--}}
{{--        <div v-if="errors.has('meeting')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('meeting') }}</div>--}}
{{--    </div>--}}
{{--</div>--}}

<div v-show="form.state > 1 && !form.is_rejected" class="form-group row align-items-center" :class="{'has-danger': errors.has('bank'), 'has-success': fields.bank && fields.bank.valid }">
    <label for="bank" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.bank') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.bank" :disabled="form.state > 2 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('bank'), 'form-control-success': fields.bank && fields.bank.valid}" id="bank" name="bank" placeholder="{{ trans('admin.application.columns.bank') }}">
        <div v-if="errors.has('bank')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bank') }}</div>
    </div>
</div>

<div v-show="form.state > 1 && !form.is_rejected" class="form-group row align-items-center" :class="{'has-danger': errors.has('payment_period'), 'has-success': fields.payment_period && fields.payment_period.valid }">
    <label for="payment_period" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.payment_period') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.payment_period" :disabled="form.state > 2 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('payment_period'), 'form-control-success': fields.payment_period && fields.payment_period.valid}" id="payment_period" name="payment_period" placeholder="{{ trans('admin.application.columns.payment_period') }}">
        <div v-if="errors.has('payment_period')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('payment_period') }}</div>
    </div>
</div>

<div v-show="form.state > 1 && !form.is_rejected" class="form-group row align-items-center" :class="{'has-danger': errors.has('commission'), 'has-success': fields.commission && fields.commission.valid }">
    <label for="commission" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.commission') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="number" v-model="form.commission" :disabled="form.state > 2 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('commission'), 'form-control-success': fields.commission && fields.commission.valid}" id="commission" name="commission" placeholder="{{ trans('admin.application.columns.commission') }}">
        <div v-if="errors.has('commission')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('commission') }}</div>
    </div>
</div>

<div v-show="form.state > 1 && !form.is_rejected" class="form-group row align-items-center" :class="{'has-danger': errors.has('min_cashback'), 'has-success': fields.min_cashback && fields.min_cashback.valid }">
    <label for="min_cashback" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.min_cashback') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="number" v-model="form.min_cashback" :disabled="form.state > 2 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('min_cashback'), 'form-control-success': fields.min_cashback && fields.min_cashback.valid}" id="min_cashback" name="min_cashback" placeholder="{{ trans('admin.application.columns.min_cashback') }}">
        <div v-if="errors.has('min_cashback')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('min_cashback') }}</div>
    </div>
</div>

<div v-show="form.state > 1 && !form.is_rejected" class="form-group row align-items-center" :class="{'has-danger': errors.has('max_cashback'), 'has-success': fields.max_cashback && fields.max_cashback.valid }">
    <label for="max_cashback" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.max_cashback') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="number" v-model="form.max_cashback" :disabled="form.state > 2 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('max_cashback'), 'form-control-success': fields.max_cashback && fields.max_cashback.valid}" id="max_cashback" name="max_cashback" placeholder="{{ trans('admin.application.columns.max_cashback') }}">
        <div v-if="errors.has('max_cashback')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('max_cashback') }}</div>
    </div>
</div>

<div v-show="form.state > 1 && !form.is_rejected" class="form-group row align-items-center" :class="{'has-danger': errors.has('discount'), 'has-success': fields.discount && fields.discount.valid }">
    <label for="discount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.discount') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="number" v-model="form.discount" :disabled="form.state > 2 || form.is_rejected == true || form.user_type == 'partner'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('discount'), 'form-control-success': fields.discount && fields.discount.valid}" id="discount" name="discount" placeholder="{{ trans('admin.application.columns.discount') }}">
        <div v-if="errors.has('discount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('discount') }}</div>
    </div>
</div>

<h3 v-show="form.state > 2 && !form.is_rejected">Offer</h3>
<font size="+1"><pre v-show="form.state > 2 && !form.is_rejected">           An offer letter was sent to the partner's email</pre></font>
<div v-show="form.state > 3 && !form.is_rejected">
<h3>Approved</h3>
<font size="+1"><pre>           An offer conditions were accepted by partner, account activated</pre></font>
</div>
<div v-show="form.state === 6 && !form.is_rejected"><h1>Process Completed. Congratulations!!!</h1></div>
<h1 v-show="form.is_rejected == true">Rejected</h1>
