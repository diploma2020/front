<div class="form-group row align-items-center" :class="{'has-danger': errors.has('company'), 'has-success': fields.company && fields.company.valid }">
    <label for="company" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.company') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.company" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('company'), 'form-control-success': fields.company && fields.company.valid}" id="company" name="company" placeholder="{{ trans('admin.application.columns.company') }}">
        <div v-if="errors.has('company')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('company') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('address'), 'has-success': fields.address && fields.address.valid }">
    <label for="address" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.address') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.address" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('address'), 'form-control-success': fields.address && fields.address.valid}" id="address" name="address" placeholder="{{ trans('admin.application.columns.address') }}">
        <div v-if="errors.has('address')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('address') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('bin'), 'has-success': fields.bin && fields.bin.valid }">
    <label for="bin" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.application.columns.bin') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.bin" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('bin'), 'form-control-success': fields.bin && fields.bin.valid}" id="bin" name="bin" placeholder="{{ trans('admin.application.columns.bin') }}">
        <div v-if="errors.has('bin')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bin') }}</div>
    </div>
</div>
