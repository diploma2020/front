<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',

            //Belongs to many relations
            'roles' => 'Roles',

        ],
    ],

    'application' => [
        'title' => 'Applications',

        'actions' => [
            'index' => 'Applications',
            'create' => 'New Application',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'uuid' => 'UUID',
            'state' => 'State',
            'company' => 'Company',
            'bin' => 'BIN',
            'partner' => 'Partner',
            'manager' => 'Manager',
            'address' => 'Address',
            'meeting' => 'Meeting',
            'bank' => 'Bank',
            'payment_period' => 'Payment period',
            'commission' => 'Commission',
            'min_cashback' => 'Min cashback',
            'max_cashback' => 'Max cashback',
            'discount' => 'Discount'
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
